# 算法（语言c++）
---

内容:  

* 字符串相关算法  
    * [KMP算法](kmp.md#1) 
    * [字符串中存在的最长的回文子串](palindrome-str.md)
* 滑动窗口
    * [数组中连续三元素之和为定值](three-el-sum.md#1)