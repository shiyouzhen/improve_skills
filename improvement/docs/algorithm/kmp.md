# KMP 算法介绍以及例子

## 算法介绍
* 此算法由Knuth，Morris和Pratt发明，故取了三位学者名字的首字母作为算法名称（你也可以以你的名字作为某个算法名称，努力啊。。。）
* 用途：字符串匹配子字符串

* 对比暴力枚举解法  
    **原串mainStr长度m,匹配传subStr长度m**;
    * 暴力解如下图，一一遍历对比，时间复杂度O((n-m)*m)，空间复杂度为O(1)
    ![暴力解](images/normal.gif)  
      
    代码实现:  

            ```c++  
            #include <iostream>  
            #include <string>  
            using namespace std;
            int32_t findSubStr(const std::string& str, const std::string& subStr)
            {
                // 如果子串为空，返回0
                if(subStr.empty())
                {
                    return 0;
                }
                // 遍历主串
                size_t i = 0;
                while(i < str.size())
                {
                    // 开始匹配子串
                    if(str[i] == subStr[0])
                    {
                        size_t j = 1;
                        for(; j < subStr.size(); j++)
                        {
                            if(str[i + j] != subStr[j]) // 匹配失败终止循环
                                break;
                        }
                        if(j == subStr.size())
                            return i;
                    }
                    i++;
                }
                // 查询不到子串
                return -1;
            }  
            ```
    * KMP主要思想，[详解KMP](https://blog.csdn.net/weixin_39811193/article/details/110804181)  
    先看图：  
    ![KMP](images/kmp.gif)  
    很明显少循环好多次，事实证明时间复杂度为O(n + m)，空间复杂度为空间复杂度：复杂度为O(m)  
        - 获取next数组 

                ```c++
                bool getNext(int* next, const std::string& subStr)
                {
                    // 二次检查有效性
                    if(next == nullptr)
                    {
                        return false;
                    }
                    // 初始化数组
                    int j = -1;
                    next[0] = j;
                    // 遍历子串，组装数组next，i = 1；开始
                    for(size_t i = 1; i < subStr.size(); i++)
                    {
                        // 1.如果前后缀不相同，即求next[i]时，需要回退直到j = -1时（也是方便匹配第一个subStr元素）;
                        while(j > -1 && subStr[i] != subStr[j + 1])
                        {
                            j = next[j];
                        }   
                        // 2.与第一种情况是互斥的关系,凡是next[i]大于-1的元素都是"连续"经过这里计算而来
                        if(subStr[i] == subStr[j + 1])
                        {
                            j++;
                        }   
                        // 赋值next[i]
                        next[i] = j;
                    }   
                    return true;
                } 
                ``` 

        - KMP算法实现   

                ```c++  
                int32_t findSubStrKMP(const std::string& str, const std::string& subStr)
                {
                    // 如果subStr = "";返回0;
                    if(subStr.empty())
                    {
                        return 0;
                    }

                    const int32_t subStrSize = subStr.size();
                    int next[subStrSize];

                    // 获取next数组
                    if(!getNext(next, subStr))
                    {
                        return -1;
                    }

                    // 开始遍历匹配
                    int32_t j = -1;
                    for(int32_t i = 0; i < str.size(); i++)
                    {
                        // 当不匹配时，应该从哪个开始匹配（找到加速匹配的位置）
                        while(j > -1 && str[i] != subStr[j + 1])
                        {
                            j = next[j];
                        }

                        // 遍历字符匹配
                        if(str[i] == subStr[j + 1])
                        {
                            j++;
                        }

                        // 匹配到子字符串时，返回匹配时第一个字符的index
                        /*
                         *
                         *                  i = 8
                         *  a a b a a b a a f c
                         *        a a b a a f
                         *     (i - j)      j = 5
                         */
                        if(j == subStrSize - 1)
                        {
                            return i - j;
                        }
                    }
                    return -1;
                }  
            ```
    

