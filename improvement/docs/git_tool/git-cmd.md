# Git 常用命令理解

Git是一个分布式版本管理系统，是为了更好地管理Linux内核开发而创立的。

### git内部管理模块理解
1、`工作区（working directory`  
2、`暂缓区（stage index`  
3、`历史记录区（history`    
![Git区域划分](images/area.jpg "Git区域划分")  


### 常用命令
- **git init**  
    * 白话就是初始化仓库，创建一个空的仓库或者重新初始化已经存在的一个git仓库(*Create an empty Git repository or reinitialize an existing one*)
---
- **git remote -v**
    * 查看远程分支地址  
    ```
    origin	git@github.com:shiyouzhen/individual_learning.git (fetch)
    origin	git@github.com:shiyouzhen/individual_learning.git (push)
    ```
---
- **git clone**  
    * 基础用法：`git clone XXX`，从远程仓库XXX下载代码
    * 添加参数：“git clone --depth=1 仓库地址 --recursive” 与 ”git clone 仓库地址“**区别：**  
    *参数1*：--depth=1代表意思是：克隆代码时只克隆最新的一次提交，即git log 查看日志日只能查看到最新的内容  
    *参数2*：--recursive代表意思是：循环递归克隆仓库的模块（submodule），`git clone XXX --recursive`等同于下面两步操作 
        * git clone；
        * git submodule update --init --recursive  
---
- **git pull**  
    * 基本用法：在仓库根目录执行git pul，从远程仓库拉取新代码
    * 本地有修改时，使用时本地最好stash一下，否则git pull时会自动执行merge（合并）操作，如果本地分支和远程同名分支相同的地方有两个人以上的更改则会有冲突，需要自己解决。    
    * 本地没有修改，会执行fast-forward操作  
---
- **git fetch**   
    * 执行pull，远程数据库的内容就会自动合并。但是，有时只是想确认本地数据库的内容而不想合并。这种情况下，使用fetch
    个人感觉最大的用途是，同步更新远程所有的分支（别的同学新建或删除已有分支），基本用法同git pull
    常用可选参数：-a: 附加到.git/FETCH_HEAD而不是覆盖 -p:修剪远程跟踪分支不在远程(git branch -a可以看到最新的分支跟踪)
    * 基本用法：git fetch (oring XXX)
    ---
    !!! note   
    git pull = git fetch + git merge
---
- **git merge**  
    - 基本用法：git merge <commit> 该命令将指定分支导入到HEAD指定的分支。
    举例：先切换master分支，然后把feature分支导入到master分支。   
    ```
    git checkout master
    git merge feature
    ```  
---
- **git rebase**   
    使用rebase可以使提交的历史记录显得更简洁
    - 基本用法：[参照链接](https://backlog.com/git-tutorial/cn/stepup/stepup2_8.html)
    如果想要rebase到远程受保护分支XXX，则可以把远程XXX分支同步到本地，然后git rebase XXX 有冲突解决冲突，然后git pus -f，最后提MR(merge request)
---  
- **git branch**
    - 基本用法：
        1. git branch #查看本地所有分支
        2. git branch -a #查看远程以及本地所有分支
        3. git branch -D（-d/--deletes） XXX #删除分支XXX（会在删除前检查merge状态，可以直接删除已经merged的分支）git branch -D 是git branch --delete/d --force/f的简写，它会直接删除  
        4. git push --delete origin XXX #删除远程分支
        5. git branch -m oldname newname #修改分支名称 （修改远程分支，需要先删除远程分支，git push origin newname、git branch --set-upstream oring/newname = git push --set-upstream origin newname）
---
- **git checkout**   
    - 基本用法：
        1. git checkout XXX #切换到本地已有分支XXX
        2. git checkout -b XXX #新建分支XXX 
        3. git checkout .(或者某个文件) #还原当前目录下所有文件(或者某个文件) 
---
- **git status**  
    - 基本用法：查看执行git status目录的文件状态  ![zhuangtai](images/status.jpg)  
    1是当前目录修改文件，可以用git restore improvement/docs/git_tool/git-cmd.md 还原修改；2是新增文件
---
- **git diff**  
    - 基本用法：
        1. git diff . #查看当前目录下所有文件和上个version的区别  
        2. git diff filename #查看某个文件与上个版本的区别 
--- 
- **git add**  
    - 基本用法： git add 文件名称/目录 #从工作区添加文件到暂缓区  
---
- **git commit** 
    - 基本用法：
        1. git commit -m "标签描述" #提交暂缓区文件到历史区  
        2. git commit --amend #修改最后一次提交的标签描述，如果要同步修改远程的描述，则修改后需要执行git push XXX -f
---
- **git push**  
    - git push #把本地历史区内容推送到远程，如果远程没有当前同名分支，可以继续执行git push --set-upstream origin XXX  
--- 
- **git log**   
查看提交历史  
用法：git log (--oneline/--graph两个可选参数)  
    * --oneline：简单显示，只有每一次提交的标签信息。如图  
    ![log-one](images/log-one.jpg "log-one") 
    * --graph: 显示分支线信息，以及提交人信息、标签和提交日期   
    ![log-graph](images/log-graph.jpg "log-one")
    * 无参数: 显示提交人信息、标签和提交日期   
    ![log](images/log.jpg "log")  

    ---  
    !!! note 
    git blame filename 查看该文件每一次的提交信息

---
- **git reset**   
常用参数:hard与soft  
    * hard（修改版本库，修改暂存区，修改工作区）  
    git --hard HEAD～1 (或是版本号)意为将版本库回退1个版本，但是不仅仅是将本地版本库的头指针全部重置到指定版本，也会重置暂存区，并且会将工作区代码也回退到这个版本
    * soft（修改版本库，保留暂存区，保留工作区）  
    git --soft HEAD~1 意为将版本库软回退1个版本，所谓软回退表示将本地版本库的头指针全部重置到指定版本，且将这次提交之后的所有变更都移动到暂存区  

---
- **git revert**  
基本用法：git revert HEAD~n (HEAD~m) (当前头指针前面第n次（到第m次）)  或者 git revert commit-id
举例：  
![gitrevert](images/revert.jpg "git revert")    
git log 标签为6的操作是新添加了6.txt文件，标签为7的提交是添加7.txt，然后执行git revert HEAD~1,修改commit标签6_1，则会把标签为6的操作操作删除，删除6.txt

与git reset 区别
```
git reset 是把HEAD向后移动了一下，而git revert是HEAD继续前进，只是新的commit的内容和要revert的内容正好相反，能够抵消要被revert的内容   
git revert与git reset最大的不同是，git revert 仅仅是撤销某次提交，而git reset会将撤销点之后的操作都回退到暂存区中。
1、git revert是用一次新的commit来回滚之前的commit，git reset是直接删除指定的commit。
2、在回滚这一操作上看，效果差不多。但是在日后继续merge以前的老版本时有区别。因为git revert是用一次逆向的commit“中和”之前的提交，因此日后合并老的branch时，导致这部分改变不会再次出现，但是git reset是之间把某些commit在某个branch上删除，因而和老的branch再次merge时，这些被回滚的commit应该还会被引入
```
---
- **git rm**  
类似Linux rm命令，故不常用git rm
```   
git rm --cached readme.txt 只从缓存区中删除readme.txt，保留物理文件  
git rm readme.txt 不但从缓存区中删除，同时删除物理文件  
git mv a.txt b.txt 把a.txt改名为b.txt  
git rm -rf *(目录:递归删除文件)  
```
