# 滑动窗口
---
滑动窗口算法是在给定特定窗口大小的数组或字符串上执行要求的操作，该技术可以将一部分问题中的嵌套循环转变为一个单循环，因此它可以减少时间复杂度  

滑动窗口的适用条件：  
1. 窗口内元素有明确的目的性：比如 窗口内元素和为target，窗口内元素不重复等；  
2. 或者窗口大小固定：比如 窗口大小为k


### 判断数组中是否存在连续的三个元素之和为特定值（示例）  

描述：给定数组[1,2,3,4,67,8,9,0,23,...],判断数组中是否存在连续三个元素之和为target  

代码实现:  

    ```c++
    int32_t sumIstarget(const int32_t* arr, const int32_t& target, const int32_t& arr_size)
    {
        if(arr_size < 3)
        {
            return -1;
        }

        // 初始化窗口大小为3
        int32_t temp_arr[3] = [arr[0], arr[1], arr[2]];
        for(int32_t i = 3; i < arr_size; i++)
        {
            if(temp_arr[0] + temp_arr[1] + temp_arr[2] == target)
            {
                return i - 3;
            }

            temp_arr[0] = temp_arr[1];
            temp_arr[1] = temp_arr[2];
            temp_arr[2] = arr[i];
        }

        // 无结果返回-1
        return -1;
    }  
    ```


算法...([详情链接][url_1])


[url_1]: https://www.cnblogs.com/blknemo/p/14474027.html